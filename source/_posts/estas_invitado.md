---
title: Estás invitado a tomar el té!
---

Este 25 de Agosto Uma te invita a tomar el té en la casa de la abuela,
Rivadavia 5558. Habrá te, café, torta, chocolate y, para apreciar mejor la
alegría de un montón de niñes corriendo y jugando por todos lados, también
habrá cerveza y vino para los mayores.

Así que vení y divertite con nosotros!

{% asset_img bailando.gif %}
